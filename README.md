# iVueit Client API Documentation

`v0.1.0`

## Overview

The **iVueit Client API** makes available the most common iVueit and data requests. The API will be expanded with additional functionality and continuously released while maintaining backward compatibilty to the maximum degree possible.

## Versioning

The **iVueit Client API** strictly adheres to [SemVer](https://semver.org/).

## Base Endpoint URL

`https://api.ivueit.com`

## Authentication

All iVueit API endpoints require authentication via JWTs.

...

## `/vues`

full endpoint: `https://api.ivueit.com/vues`

The `/vues` endpoint provides completed vue metadata, photos, and PDFs. Media assets are delivered via signed AWS S3 links.

### Options

...

### Examples

...
